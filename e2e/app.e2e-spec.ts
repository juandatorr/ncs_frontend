import { NcsPage } from './app.po';

describe('ncs App', () => {
  let page: NcsPage;

  beforeEach(() => {
    page = new NcsPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
