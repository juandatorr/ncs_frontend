import { TestBed, inject } from '@angular/core/testing';

import { NcsCrudService } from './ncs-crud.service';

describe('NcsCrudService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NcsCrudService]
    });
  });

  it('should be created', inject([NcsCrudService], (service: NcsCrudService) => {
    expect(service).toBeTruthy();
  }));
});
