import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, ResponseContentType } from '@angular/http';

import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/first';
import 'rxjs/add/operator/catch';

const BASE_URL = 'http://localhost:9000/';

@Injectable()
export class NcsCrudService {

  /**
   * Campos del servicio.
  */
      private Jsons: any; // Variable que contiene el json con los datos a enviar.
      private headers = new Headers({ 'Content-Type': 'application/json' }); // Constante que asigna los valores necesarios a los headers para realizar la transacción.
      private options = new RequestOptions({ headers: this.headers }); // Constante que contiene las opciones necesarias para realizar una transacción con el backend.
 
    /**
     * Constructor del servicio.
     * @param {Http} http Variable que permite la comunicacion con el servidor.
     */
    constructor( private http: Http ) { }

    agregar(modelo: string, valor: any) {
        this.Jsons = JSON.stringify(valor);
        console.log(this.Jsons);
        return this.http.post(BASE_URL + modelo, valor, this.options)
            .map(r =>  r)
            .catch(this.handleError);
    }

    listas(modelo: string){
        return this.http.get(BASE_URL + modelo, this.options)
          .map((response: Response) => response.json());
    }

    consultar(modelo: string, id: any){
        return this.http.get(BASE_URL + modelo + "/'" + id + "'")
          .map((response: Response) => response.json());
    }

    consultarGenero(modelo: string){
        return this.http.get(BASE_URL + modelo)
          .map((response: Response) => response.json());
    }
    eliminar(modelo: string, prueba: any,  id: any){
        this.Jsons = JSON.stringify({prueba});
        console.log(this.Jsons);
        return this.http.patch(BASE_URL + modelo + '/' + id, this.Jsons, this.options)
          .map(r =>  r)
          .catch(this.handleError);
    }

    actualizar(modelo: string, valor: any, id: any){
        this.Jsons = JSON.stringify({valor});
        return this.http.patch(BASE_URL + modelo + "/'" + id + "'", this.Jsons, this.options)
          .map((response: Response) => response);
    }

    login(usuario:any, clave:any){
        return this.http.get(BASE_URL + "login/'" + usuario + "'/'" + clave + "'")
            .map((response: Response) => response.json());
    }

    /**
     * Función que dá formato al error obtenido desde el backend.
     * @param {any} error Variable que contiene toda la informacion del error generado desde el backend.
     */
    private handleError(error: Response | any) {
        let errMsg: string;
        if ( error instanceof Response ) {
            let body = error.json() || '';
            let err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        return Observable.throw(errMsg);
    }

}
