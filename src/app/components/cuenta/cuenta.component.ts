import { Component, OnInit } from '@angular/core';
import { NcsCrudService } from '../../providers/ncs-crud.service';
import { Cookie } from 'ng2-cookies/ng2-cookies';


@Component({
  selector: 'app-cuenta',
  templateUrl: './cuenta.component.html',
  styleUrls: ['./cuenta.component.css']
})
export class CuentaComponent implements OnInit {

  cuenta= {correoElectronico:'any', primerNombre:'any', segundoNombre:'any', primerApellido:'any', segundoApellido:'any', direccion: 'any', numCelular: 'any'};
  clave={clave : 'any'};
  constructor(
    private servicio: NcsCrudService
  ) {
        this.consultar(Cookie.get('usuario'));
        this.consultarClave(Cookie.get('usuario'));
         }

  ngOnInit() {
    this.consultar(Cookie.get('usuario'));
    this.consultarClave(Cookie.get('usuario'));
  }

  consultar(correoElectronico : any){
      this.servicio.consultar('listarTercero', correoElectronico)
      .subscribe(
        respuesta => {
            console.log(respuesta);
            
              this.cuenta = respuesta;
        }, error => {
            console.log(error);
        }
      );
  }

  consultarClave(correoElectronico : any){
    this.servicio.consultar('darClave' , correoElectronico)
    .subscribe(
       respuesta => {
              this.clave = respuesta;
        }, error => {
            console.log(error);
        }
    );
  }

  actualizarCuenta(correoElectronico:any, primerNombre:any, segundoNombre:any, primerApellido:any, segundoApellido:any, direccion: any, numCelular: any, clave: any){
    this.servicio.actualizar('actualizarTercero',{correoElectronico, primerNombre, segundoNombre, primerApellido, segundoApellido,direccion,numCelular, idEstado : 1},  correoElectronico )
    .subscribe(
        respuesta => {
        }, error => {
            console.log(error);
        }
    );
    this.servicio.actualizar('actualizarUsuario', {clave}, correoElectronico).subscribe(
       respuesta => {
        }, error => {
            console.log(error);
        }
    );
  }

  eliminarCuenta(correoElectronico:any){
    console.log(correoElectronico);
    this.servicio.actualizar('eliminarTercero', {correoElectronico}, correoElectronico)
    .subscribe(
        respuesta => {
        }, error => {
            console.log(error);
        }
    );

}
comprobar(){
  if(typeof(this.cuenta) === 'undefined' && typeof(this.clave) === 'undefined' ){
    return true;
  }
  return false;
}
}
