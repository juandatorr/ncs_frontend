import { Component, OnInit, Output, EventEmitter} from '@angular/core';
import { NcsCrudService } from '../../providers/ncs-crud.service';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  @Output()
    private ingresar = new EventEmitter<boolean>();

    roles : any;
  constructor(
        private servicio: NcsCrudService
  ) { 
    this.cargarCombo()
  }

  enviarEstado(){
      this.ingresar.emit(true);
  }
  ngOnInit() {
    this.cargarCombo()
  }

  agregar(correoElectronico:any, primerNombre:any, segundoNombre:any, primerApellido:any, segundoApellido:any, direccion: any, numCelular: any, clave: any, idRol:any){
    this.servicio.agregar('agregarTercero', {correoElectronico, primerNombre, segundoNombre, primerApellido, segundoApellido,direccion,numCelular, ncsEstadosByIdEstado:{idEstado:1}})
    .subscribe(
      respuesta => {
      }, error => {
          console.log(error);
      }
    );
    this.servicio.agregar('agregarUsuario', {correoElectronico, clave, ncsEstadosByIdEstado:{idEstado:1}, ncsRolesByIdRol:{idRol}})
    .subscribe(
      respuesta => {
      }, error => {
          console.log(error);
      }
    )
}

  cargarCombo(){
    this.servicio.listas('listarRoles').subscribe(
        respuesta =>{
          console.log(respuesta);
        this.roles = respuesta; 
          if(respuesta === "Error"){
            console.log("No existen roles");
        }
      }, error => {
          console.log(error);
      }
    );
  }
}
