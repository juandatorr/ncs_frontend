import { Component, OnInit } from '@angular/core';
import { NcsCrudService } from '../../providers/ncs-crud.service';
import { Cookie } from 'ng2-cookies/ng2-cookies';

@Component({
  selector: 'app-disco',
  templateUrl: './disco.component.html',
  styleUrls: ['./disco.component.css']
})
export class DiscoComponent implements OnInit {

  model : any;
  fechaString : any;
  discos : any;
  constructor(
    private servicio: NcsCrudService
  ) {
      this.discosArtistas(Cookie.get('usuario'));
   }

  ngOnInit() {
      this.discosArtistas(Cookie.get('usuario'));
  }

  agregar(nomDisco, fLanzamiento, descDisco){
   this.fechaString = this.model.year + '-';
        if (this.model.month <= 9) {
            this.fechaString += '0' + this.model.month + '-';
        }else {
            this.fechaString += this.model.month + '-';
        }
        if (this.model.day <= 9) {
            this.fechaString += '0' + this.model.day;
        }else {
            this.fechaString += this.model.day;
        }
       
    this.servicio.agregar('agregarDisco', {idDisco : 1, nomDisco, fLanzamiento : this.fechaString, descDisco,  idEstado: 1,  correoElectronico: Cookie.get('usuario')})
    .subscribe(
      respuesta => {        
      }, error => {
          console.log(error);
      }
    );
}

    discosArtistas(correoElectronico : any){
        this.servicio.consultar('discosArtista', correoElectronico).
        subscribe(
        respuesta => {
            this.discos = respuesta;        
            }, error => {
                console.log(error);
            }
        );
    }


    eliminarDisco(idDisco : any){
        this.servicio.eliminar('eliminarDisco', 5, idDisco)
        .subscribe(
        respuesta => {
                
        }, error => {
            console.log("aqui")
                console.log(error);
            }
        );
    }

}
