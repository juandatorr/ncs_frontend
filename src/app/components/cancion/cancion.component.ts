import { Component, OnInit } from '@angular/core';
import { NcsCrudService } from '../../providers/ncs-crud.service';
import { Cookie } from 'ng2-cookies/ng2-cookies';


@Component({
  selector: 'app-cancion',
  templateUrl: './cancion.component.html',
  styleUrls: ['./cancion.component.css']
})
export class CancionComponent implements OnInit {

  discos : any;
  generos: any;
  constructor(
    private servicio: NcsCrudService
  ) { 
    this.cargarComboDiscos(Cookie.get('Usuario'));
    this.cargarComboGeneros();
  }

  ngOnInit() {
    this.cargarComboDiscos(Cookie.get('Usuario'));
    this.cargarComboGeneros();

  }

cargarComboDiscos(correoElectronico : any){
    this.servicio.consultar('discosArtista', correoElectronico).subscribe(
        respuesta =>{
          console.log(respuesta);
        this.discos = respuesta; 
          if(respuesta === "Error"){
            console.log("No existen roles");
        }
      }, error => {
          console.log(error);
      }
    );
  }

  cargarComboGeneros(){
    this.servicio.consultarGenero('listarGeneros').subscribe(
        respuesta =>{
        this.generos = respuesta; 
          if(respuesta === "Error"){
            console.log("No existen roles");
        }
      }, error => {
          console.log(error);
      }
    );
  }
}
