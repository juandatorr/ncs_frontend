import { Component, OnInit, Output,EventEmitter } from '@angular/core';
import { NcsCrudService } from '../../providers/ncs-crud.service';
import { Router } from '@angular/router';
import { Cookie } from 'ng2-cookies/ng2-cookies';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  @Output()
  private cambiar = new EventEmitter<boolean>();
  
  @Output()
  private log = new EventEmitter<boolean>();


  constructor(
     private servicio: NcsCrudService,
     private router: Router
  ) { 

  }

  login(usuario: any, clave: any ){
    this.servicio.login(usuario, clave)
    .subscribe(
    respuesta => {
        console.log(respuesta);
        Cookie.set('usuario', respuesta["0"].CORREO_ELECTRONICO);
        this.logEstado();
        this.router.navigate(['/menu']);
        
      }, error => {
          console.log(error);
      }
    );
  }

  enviarEstado(){
      this.cambiar.emit(false);
  }

  logEstado(){
      this.log.emit(false);
  }
  ngOnInit() {

  }


}
