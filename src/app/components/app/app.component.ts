import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  ingresar = true;
  ingreso = true;

  constructor(
      private router: Router,
      private activatedRoute : ActivatedRoute
  ){
  }
   cambiarEstado(ingresar: boolean){
      this.ingresar = ingresar;
  }

    ingreso_login(ingreso:boolean){
      this.ingreso = ingreso;
  }
  
}


