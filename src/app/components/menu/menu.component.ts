import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  constructor(
      private router: Router,
      private activatedRoute : ActivatedRoute
  ) { }

  ngOnInit() {
    
  }

}
