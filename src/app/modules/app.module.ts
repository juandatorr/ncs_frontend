import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from '../components/app/app.component';
import { LoginComponent} from '../components/login/login.component';
import { RegistroComponent } from '../components/registro/registro.component';
import { CancionComponent } from '../components/cancion/cancion.component';
import { NcsCrudService } from '../providers/ncs-crud.service';
import { HttpModule, JsonpModule } from '@angular/http';
import { Routing } from '../app.routing';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MenuComponent } from '../components/menu/menu.component';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { DiscoComponent } from '../components/disco/disco.component';
import { CuentaComponent } from '../components/cuenta/cuenta.component';
import { ListaCancionesComponent } from '../components/lista-canciones/lista-canciones.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegistroComponent,
    CancionComponent,
    MenuComponent,
    DiscoComponent,
    CuentaComponent,
    ListaCancionesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    JsonpModule,
    Routing, 
    NgbModule.forRoot()
  ],
  providers: [NcsCrudService],
  bootstrap: [AppComponent]
})
export class AppModule { }
