import { Routes, RouterModule } from '@angular/router';

import { MenuComponent } from './components/menu/menu.component'; 
import { AppComponent } from './components/app/app.component';                        
                       
const appRoutes = [
{ path: 'menu', component: MenuComponent },
{ path: 'login', component: AppComponent }
//{ path: '', redirectTo: 'login', pathMatch: 'full' }
];

export const Routing = RouterModule.forRoot(appRoutes);